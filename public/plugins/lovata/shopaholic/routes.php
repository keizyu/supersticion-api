<?php

use Lovata\Shopaholic\Models\Product;
use Lovata\Shopaholic\Models\Category;
use Lovata\Shopaholic\Models\PromoBlock;


Route::middleware(['api'])->group(function(){
    Route::get('catalogo', function(){

        // $catalogo = Product::all();
        $catalogo = Product::with(['preview_image','images','offer','additional_category','category'])->get();
        return $catalogo;

    });

    Route::get('categoria', function(){

        $categoria = Category::all();
        return $categoria;

    });
});
