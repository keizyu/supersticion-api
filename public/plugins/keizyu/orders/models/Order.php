<?php namespace Keizyu\Orders\Models;

use Model;

/**
 * Model
 */
class Order extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'keizyu_orders_1';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
