<?php namespace Keizyu\Orders\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKeizyuOrders14 extends Migration
{
    public function up()
    {
        Schema::rename('keizyu_orders_', 'keizyu_orders_1');
    }
    
    public function down()
    {
        Schema::rename('keizyu_orders_1', 'keizyu_orders_');
    }
}
