<?php namespace Keizyu\Orders\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteKeizyuOrders extends Migration
{
    public function up()
    {
        Schema::dropIfExists('keizyu_orders_');
    }
    
    public function down()
    {
        Schema::create('keizyu_orders_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 191);
            $table->string('size', 191)->nullable();
            $table->integer('quantity');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
}
