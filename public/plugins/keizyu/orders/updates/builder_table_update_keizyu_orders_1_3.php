<?php namespace Keizyu\Orders\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKeizyuOrders13 extends Migration
{
    public function up()
    {
        Schema::table('keizyu_orders_1', function($table)
        {
            $table->string('address')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('keizyu_orders_1', function($table)
        {
            $table->dropColumn('address');
        });
    }
}
