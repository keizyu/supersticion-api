<?php namespace Keizyu\Orders\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKeizyuOrders12 extends Migration
{
    public function up()
    {
        Schema::table('keizyu_orders_1', function($table)
        {
            $table->string('phone', 10)->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('keizyu_orders_1', function($table)
        {
            $table->integer('phone')->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
