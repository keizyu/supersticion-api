<?php namespace Keizyu\Orders\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKeizyuOrders1 extends Migration
{
    public function up()
    {
        Schema::create('keizyu_orders_1', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('email')->nullable();
            $table->integer('phone')->nullable();
            $table->double('price', 10, 0)->nullable();
            $table->string('payment_method')->nullable();
            $table->text('order_detail')->nullable();
            $table->text('user_id')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('keizyu_orders_1');
    }
}
