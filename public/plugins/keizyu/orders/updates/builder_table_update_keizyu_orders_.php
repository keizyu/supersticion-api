<?php namespace Keizyu\Orders\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKeizyuOrders extends Migration
{
    public function up()
    {
        Schema::rename('keizyu_orders_1', 'keizyu_orders_');
    }
    
    public function down()
    {
        Schema::rename('keizyu_orders_', 'keizyu_orders_1');
    }
}
