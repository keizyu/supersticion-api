<?php

use Keizyu\Orders\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User;

Route::middleware(['api','jwt.auth'])->group(function(){

    Route::post('api/create-order', function(Request $req){

        $order = new Order;
        $order -> name = $req -> name;
        $order -> email = $req -> email;
        $order -> phone = $req -> phone;
        $order -> price = $req -> price;
        $order -> payment_method = $req -> payment_method;
        $order -> order_detail = $req -> order_detail;
        $order -> address = $req -> address;
        $order -> user_id = $req -> user_id;

        $order->save();

        return response()->json('Order Created');


    });

});

Route::get('api/user-orders', function(){
    $users = RainLab\User\Models\User::where('id', '>', 0)
                    ->with('orders')
                        ->get();

    return $users;
});
