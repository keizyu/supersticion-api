<?php namespace Keizyu\Orders;

use System\Classes\PluginBase;
use RainLab\User\Models\User;

class Plugin extends PluginBase
{
    public function boot()
	{
	    \RainLab\User\Models\User::extend(function($model) {
        	$model->hasMany['orders'] = 'Keizyu\Orders\Models\Order';
	    });

	}
    
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }
}
